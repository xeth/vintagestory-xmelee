using System;
using System.Text;
using Vintagestory.API.Client;
using Vintagestory.API.Common;
using Vintagestory.API.Common.Entities;
using Vintagestory.API.Config;
using Vintagestory.API.MathTools;
using Vintagestory.API.Util;

namespace XMelee
{

    // pike, built in:
    // - no parry
    // - no attack with shield
    // 
    // TODO: when horses, pike right click intended for raising pike wall
    public class ItemPike : Item
    {
        public static string NAME { get; } = "ItemPike";

        // attack mechanics:
        public static float DEFAULT_ATTACK_DELAY = 0.2f;
        public static float DEFAULT_ATTACK_WINDOW = 0.7f;
        public static float DEFAULT_ATTACK_TIME = 1.0f;
        public static float DEFAULT_ATTACK_WALK_SPEED_MODIFIER = -1f;

        // animations
        public static String DEFAULT_IDLE_ANIMATION = "holdinglanternrighthand";
        public static String DEFAULT_ATTACK_ANIMATION = "spearhit";


        public override string GetHeldTpIdleAnimation(ItemSlot activeHotbarSlot, Entity forEntity, EnumHand hand)
        {
            return DEFAULT_IDLE_ANIMATION;
        }

        public override string GetHeldTpUseAnimation(ItemSlot activeHotbarSlot, Entity byEntity)
        {
            return null;
        }

        public override string GetHeldTpHitAnimation(ItemSlot activeHotbarSlot, Entity byEntity)
        {
            return null;
        }

        public override void OnHeldInteractStart(ItemSlot itemslot, EntityAgent byEntity, BlockSelection blockSel, EntitySelection entitySel, bool firstEvent, ref EnumHandHandling handling)
        {
            handling = EnumHandHandling.PreventDefault;
        }

        public override bool OnHeldInteractStep(float secondsUsed, ItemSlot itemslot, EntityAgent byEntity, BlockSelection blockSel, EntitySelection entitySel)
        {
            return false;
        }


        public override bool OnHeldInteractCancel(float secondsUsed, ItemSlot itemslot, EntityAgent byEntity, BlockSelection blockSel, EntitySelection entitySel, EnumItemUseCancelReason cancelReason)
        {
            return false;
        }

        public override void OnHeldInteractStop(float secondsUsed, ItemSlot itemslot, EntityAgent byEntity, BlockSelection blockSel, EntitySelection entitySel)
        {

        }


        public override void OnHeldAttackStart(ItemSlot itemslot, EntityAgent byEntity, BlockSelection blockSel, EntitySelection entitySel, ref EnumHandHandling handling)
        {
            // check if weapon can attack with shield
            if ( XMeleeMod.IsUsingShield(byEntity) )
            {
                (byEntity.World.Api as ICoreClientAPI)?.TriggerIngameError(this, "ingameerror-attack-with-shield", Lang.Get("xmelee:ingameerror-attack-with-shield"));
                handling = EnumHandHandling.PreventDefault;
                return;
            }

            float attackDelay = DEFAULT_ATTACK_DELAY;
            float attackWindow = DEFAULT_ATTACK_WINDOW;
            float attackTime = DEFAULT_ATTACK_TIME;
            float walkspeedModifier = DEFAULT_ATTACK_WALK_SPEED_MODIFIER;
            if (itemslot.Itemstack.Collectible.Attributes != null)
            {
                attackDelay = itemslot.Itemstack.Collectible.Attributes["attackDelay"].AsFloat(DEFAULT_ATTACK_DELAY);
                attackWindow = itemslot.Itemstack.Collectible.Attributes["attackWindow"].AsFloat(DEFAULT_ATTACK_WINDOW);
                attackTime = itemslot.Itemstack.Collectible.Attributes["attackTime"].AsFloat(DEFAULT_ATTACK_TIME);
                walkspeedModifier = itemslot.Itemstack.Collectible.Attributes["attackWalkspeed"].AsFloat(DEFAULT_ATTACK_WALK_SPEED_MODIFIER);
            }

            int attackDelayMilliseconds = (int) (attackDelay * 1000f);
            int attackWindowMilliseconds = (int) (attackWindow * 1000f);
            int attackTimeMilliseconds = (int) (attackTime * 1000f);

            byEntity.StartAnimation(base.GetHeldTpHitAnimation(itemslot, byEntity));
            byEntity.Attributes.SetInt("didattack", 0);
            byEntity.Stats.Set("walkspeed", "xmelee", walkspeedModifier, true); // slow movement while attacking

            // play strike sound
            byEntity.World.RegisterCallback((dt) =>
            {
                IPlayer byPlayer = (byEntity as EntityPlayer).Player;
                if (byPlayer == null) return;

                if (byEntity.Controls.HandUse == EnumHandInteract.HeldItemAttack)
                {
                    byPlayer.Entity.World.PlaySoundAt(new AssetLocation("sounds/player/strike"), byPlayer.Entity, byPlayer, 0.9f + (float)api.World.Rand.NextDouble() * 0.2f, 16, 1f);
                }
            }, attackDelayMilliseconds);

            // disable animation after window
            byEntity.World.RegisterCallback((dt) => {
                byEntity.StopAnimation(base.GetHeldTpHitAnimation(itemslot, byEntity));
            }, attackWindowMilliseconds);

            // finish attack
            byEntity.World.RegisterCallback((dt) =>
            {
                byEntity.Stats.Set("walkspeed", "xmelee", 0f, true);
                byEntity.Attributes.SetInt("didattack", 1);
            }, attackTimeMilliseconds);

            handling = EnumHandHandling.PreventDefault;
        }

        public override bool OnHeldAttackCancel(float secondsUsed, ItemSlot itemslot, EntityAgent byEntity, BlockSelection blockSelection, EntitySelection entitySel, EnumItemUseCancelReason cancelReason)
        {
            return false;
        }

        /**
         * Hit any entity in front of player until swing is over
         */
        public override bool OnHeldAttackStep(float secondsUsed, ItemSlot itemslot, EntityAgent byEntity, BlockSelection blockSelection, EntitySelection entitySel)
        {
            // check if using shield
            if ( XMeleeMod.IsUsingShield(byEntity) )
            {
                (byEntity.World.Api as ICoreClientAPI)?.TriggerIngameError(this, "ingameerror-attack-with-shield", Lang.Get("xmelee:ingameerror-attack-with-shield"));
                return false;
            }
            
            float attackDelay = DEFAULT_ATTACK_DELAY;
            float attackWindow = DEFAULT_ATTACK_WINDOW;
            float attackTime = DEFAULT_ATTACK_TIME;
            if (itemslot.Itemstack.Collectible.Attributes != null)
            {
                attackDelay = itemslot.Itemstack.Collectible.Attributes["attackDelay"].AsFloat(DEFAULT_ATTACK_DELAY);
                attackWindow = itemslot.Itemstack.Collectible.Attributes["attackWindow"].AsFloat(DEFAULT_ATTACK_WINDOW);
                attackTime = itemslot.Itemstack.Collectible.Attributes["attackTime"].AsFloat(DEFAULT_ATTACK_TIME);
            }

            if (byEntity.World.Side == EnumAppSide.Client)
            {
                // time in attack motion
                float t0 = Math.Min(1f, secondsUsed/attackDelay); // ease-in
                float t1 = XMeleeMod.AnimationEnvelope(secondsUsed, attackDelay, attackDelay + 0.14f); // main
                float t2 = XMeleeMod.AnimationEnvelope(secondsUsed, attackTime - 0.15f, attackTime); // ease-out (~15%)

                // model animation
                IClientWorldAccessor world = byEntity.World as IClientWorldAccessor;
                ModelTransform tf = new ModelTransform();
                tf.EnsureDefaultValues();

                tf.Origin.Set(0.5f, 0f, 0.5f);
                tf.Translation.Set(
                    t1 * 0.3f + t2 * -0.3f,
                    t0 * -0.1f + t1 * 0.4f + t2 * -0.3f,
                    t0 * -1.05f + t2 * 1.05f
                );
                tf.Rotation.Set(
                    0f,
                    0f,
                    t0 * -5f + t1 * 10f + t2 * -5f
                );

                byEntity.Controls.UsingHeldItemTransformBefore = tf;

                // do damage
                if (secondsUsed > attackDelay && secondsUsed < attackWindow)
                {
                    if ( entitySel != null && byEntity.Attributes.GetInt("didattack") == 0 )
                    {
                        world.TryAttackEntity(entitySel);
                        byEntity.Attributes.SetInt("didattack", 1); // mark attacked
                    }
                }
            }

            return secondsUsed < attackTime;
        }

        public override void OnHeldAttackStop(float secondsUsed, ItemSlot itemslot, EntityAgent byEntity, BlockSelection blockSelection, EntitySelection entitySel)
        {

        }


        public override void GetHeldItemInfo(ItemSlot itemslot, StringBuilder dsc, IWorldAccessor world, bool withDebugInfo)
        {
            base.GetHeldItemInfo(itemslot, dsc, world, withDebugInfo);
            if (itemslot.Itemstack.Collectible.Attributes == null) return;

            float attackDelay = DEFAULT_ATTACK_DELAY;
            float attackWindow = DEFAULT_ATTACK_WINDOW;
            float attackTime = DEFAULT_ATTACK_TIME;
            float attackWalkspeed = DEFAULT_ATTACK_WALK_SPEED_MODIFIER;
            if ( itemslot.Itemstack.Collectible.Attributes != null )
            {
                attackDelay = itemslot.Itemstack.Collectible.Attributes["attackDelay"].AsFloat(DEFAULT_ATTACK_DELAY);
                attackWindow = itemslot.Itemstack.Collectible.Attributes["attackWindow"].AsFloat(DEFAULT_ATTACK_WINDOW);
                attackTime = itemslot.Itemstack.Collectible.Attributes["attackTime"].AsFloat(DEFAULT_ATTACK_TIME);
                attackWalkspeed = itemslot.Itemstack.Collectible.Attributes["attackWalkspeed"].AsFloat(DEFAULT_ATTACK_WALK_SPEED_MODIFIER);
            }

            dsc.AppendLine(Lang.Get("xmelee:desc-attack-time") + String.Format(": {0}/{1}/{2} s", attackDelay, attackWindow, attackTime));
            dsc.AppendLine(Lang.Get("xmelee:desc-attack-walkspeed") + ": " + attackWalkspeed);
        }

        public override WorldInteraction[] GetHeldInteractionHelp(ItemSlot itemslot)
        {
            return new WorldInteraction[] {
                new WorldInteraction()
                {
                    ActionLangCode = "heldhelp-throw",
                    MouseButton = EnumMouseButton.Right,
                }
            }.Append(base.GetHeldInteractionHelp(itemslot));
        }
    }
}
