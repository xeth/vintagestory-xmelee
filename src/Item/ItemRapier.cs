using System;
using System.Text;
using Vintagestory.API.Client;
using Vintagestory.API.Common;
using Vintagestory.API.Common.Entities;
using Vintagestory.API.Config;
using Vintagestory.API.MathTools;
using Vintagestory.API.Util;

namespace XMelee
{

    public class ItemRapier : Item
    {
        public static string NAME { get; } = "ItemRapier";

        // attack mechanics:
        public static float DEFAULT_ATTACK_DELAY = 0.2f;
        public static float DEFAULT_ATTACK_WINDOW = 0.7f;
        public static float DEFAULT_ATTACK_TIME = 1.0f;
        public static float DEFAULT_ATTACK_WALK_SPEED_MODIFIER = -1f;
        public static bool DEFAULT_CAN_ATTACK_WITH_SHIELD = true;

        // parry mechanics:
        public static float DEFAULT_PARRY_DELAY = 0.1f; // time before parry starts
        public static float DEFAULT_PARRY_WINDOW = 0.7f; // parry: delay < t < parry window
        public static float DEFAULT_PARRY_TIME = 1.0f; // total parry animation time
        public static float DEFAULT_PARRY_WALK_SPEED_MODIFIER = -0.4f; // reduce walk speed while parrying
        public static bool DEFAULT_CAN_PARRY_WITH_SHIELD = true;

        // animations
        public static String DEFAULT_ATTACK_ANIMATION = "axechop";
        public static String DEFAULT_PARRY_ANIMATION = "shoveldig";


        public override string GetHeldTpUseAnimation(ItemSlot activeHotbarSlot, Entity byEntity)
        {
            return null;
        }

        public override string GetHeldTpHitAnimation(ItemSlot activeHotbarSlot, Entity byEntity)
        {
            return null;
        }

        public override void OnHeldInteractStart(ItemSlot itemslot, EntityAgent byEntity, BlockSelection blockSel, EntitySelection entitySel, bool firstEvent, ref EnumHandHandling handling)
        {
            String parryAnimation = DEFAULT_PARRY_ANIMATION;
            float parryDelay = DEFAULT_PARRY_DELAY;
            float parryWindow = DEFAULT_PARRY_WINDOW;
            float parryTime = DEFAULT_PARRY_TIME;
            float walkspeedModifier = DEFAULT_PARRY_WALK_SPEED_MODIFIER;
            bool canParryWithShield = DEFAULT_CAN_PARRY_WITH_SHIELD;
            if (itemslot.Itemstack.Collectible.Attributes != null)
            {
                parryDelay =  itemslot.Itemstack.Collectible.Attributes["parryDelay"].AsFloat(DEFAULT_PARRY_DELAY);
                parryWindow =  itemslot.Itemstack.Collectible.Attributes["parryWindow"].AsFloat(DEFAULT_PARRY_WINDOW);
                parryTime =  itemslot.Itemstack.Collectible.Attributes["parryTime"].AsFloat(DEFAULT_PARRY_TIME);
                parryAnimation = itemslot.Itemstack.Collectible.Attributes["parryAnimation"].AsString(DEFAULT_PARRY_ANIMATION);
                walkspeedModifier = itemslot.Itemstack.Collectible.Attributes["parryWalkspeed"].AsFloat(DEFAULT_PARRY_WALK_SPEED_MODIFIER);
                canParryWithShield = itemslot.Itemstack.Collectible.Attributes["canParryWithShield"].AsBool(DEFAULT_CAN_PARRY_WITH_SHIELD);
            }

            // check if weapon can parry with shield
            if ( canParryWithShield == false && XMeleeMod.IsUsingShield(byEntity) )
            {
                (byEntity.World.Api as ICoreClientAPI)?.TriggerIngameError(this, "ingameerror-parry-with-shield", Lang.Get("xmelee:ingameerror-parry-with-shield"));
                handling = EnumHandHandling.PreventDefault;
                return;
            }

            byEntity.StartAnimation(parryAnimation);
            byEntity.Stats.Set("walkspeed", "xmelee", walkspeedModifier, true);

            // enable parry after delay
            byEntity.World.RegisterCallback((dt) => {
                byEntity.Attributes.SetInt("parry", 1);
            }, (int) (parryDelay * 1000f));

            // disable parry after window + stop animation
            byEntity.World.RegisterCallback((dt) => {
                byEntity.StopAnimation(parryAnimation);
                byEntity.Attributes.SetInt("parry", 0);
            }, (int) (parryWindow * 1000f));

            // allow movement again
            byEntity.World.RegisterCallback((dt) => {
                byEntity.Stats.Set("walkspeed", "xmelee", 0f, true);
            }, (int) (parryTime * 1000f));

            handling = EnumHandHandling.PreventDefault;
        }

        public override bool OnHeldInteractStep(float secondsUsed, ItemSlot itemslot, EntityAgent byEntity, BlockSelection blockSel, EntitySelection entitySel)
        {
            float parryDelay = DEFAULT_ATTACK_DELAY;
            float parryWindow = DEFAULT_ATTACK_WINDOW;
            float parryTime = DEFAULT_ATTACK_TIME;
            bool canParryWithShield = DEFAULT_CAN_PARRY_WITH_SHIELD;
            if (itemslot.Itemstack.Collectible.Attributes != null)
            {
                parryDelay = itemslot.Itemstack.Collectible.Attributes["parryDelay"].AsFloat(DEFAULT_PARRY_DELAY);
                parryWindow = itemslot.Itemstack.Collectible.Attributes["parryWindow"].AsFloat(DEFAULT_PARRY_WINDOW);
                parryTime = itemslot.Itemstack.Collectible.Attributes["parryTime"].AsFloat(DEFAULT_PARRY_TIME);
                canParryWithShield = itemslot.Itemstack.Collectible.Attributes["canParryWithShield"].AsBool(DEFAULT_CAN_PARRY_WITH_SHIELD);
            }

            // check if weapon can parry with shield
            if ( canParryWithShield == false && XMeleeMod.IsUsingShield(byEntity) )
            {
                (byEntity.World.Api as ICoreClientAPI)?.TriggerIngameError(this, "ingameerror-parry-with-shield", Lang.Get("xmelee:ingameerror-parry-with-shield"));
                return false;
            }

            if (byEntity.World.Side == EnumAppSide.Client)
            {
                // time in attack motion
                float t0 = Math.Min(1f, secondsUsed/parryDelay); // ease-in
                float t1 = XMeleeMod.AnimationEnvelope(secondsUsed, parryDelay, parryDelay + 0.1f); // main
                float t2 = XMeleeMod.AnimationEnvelope(secondsUsed, parryTime - 0.1f, parryTime); // ease-out (~15%)

                // model animation
                IClientWorldAccessor world = byEntity.World as IClientWorldAccessor;
                ModelTransform tf = new ModelTransform();
                tf.EnsureDefaultValues();

                tf.Origin.Set(0.5f, 0f, 0.5f);
                tf.Translation.Set(
                    t1 * -0.1f + t2 * 0.1f,
                    t0 * 0.3f + t2 * -0.3f,
                    t0 * -0.2f + t2 * 0.2f
                );
                tf.Rotation.Set(
                    t0 * 30f + t2 * -60f,
                    t0 * -10f + t1 * 90f + t2 * -60f,
                    t0 * 10f + t1 * -20f
                );

                byEntity.Controls.UsingHeldItemTransformBefore = tf;
            }

            return secondsUsed < parryTime;
        }


        public override bool OnHeldInteractCancel(float secondsUsed, ItemSlot itemslot, EntityAgent byEntity, BlockSelection blockSel, EntitySelection entitySel, EnumItemUseCancelReason cancelReason)
        {
            return false;
        }

        public override void OnHeldInteractStop(float secondsUsed, ItemSlot itemslot, EntityAgent byEntity, BlockSelection blockSel, EntitySelection entitySel)
        {

        }


        public override void OnHeldAttackStart(ItemSlot itemslot, EntityAgent byEntity, BlockSelection blockSel, EntitySelection entitySel, ref EnumHandHandling handling)
        {
            float attackDelay = DEFAULT_ATTACK_DELAY;
            float attackWindow = DEFAULT_ATTACK_WINDOW;
            float attackTime = DEFAULT_ATTACK_TIME;
            float walkspeedModifier = DEFAULT_ATTACK_WALK_SPEED_MODIFIER;
            bool canAttackWithShield = DEFAULT_CAN_ATTACK_WITH_SHIELD;
            if ( itemslot.Itemstack.Collectible.Attributes != null )
            {
                attackDelay = itemslot.Itemstack.Collectible.Attributes["attackDelay"].AsFloat(DEFAULT_ATTACK_DELAY);
                attackWindow = itemslot.Itemstack.Collectible.Attributes["attackWindow"].AsFloat(DEFAULT_ATTACK_WINDOW);
                attackTime = itemslot.Itemstack.Collectible.Attributes["attackTime"].AsFloat(DEFAULT_ATTACK_TIME);
                walkspeedModifier = itemslot.Itemstack.Collectible.Attributes["attackWalkspeed"].AsFloat(DEFAULT_ATTACK_WALK_SPEED_MODIFIER);
                canAttackWithShield = itemslot.Itemstack.Collectible.Attributes["canAttackWithShield"].AsBool(DEFAULT_CAN_ATTACK_WITH_SHIELD);
            }

            // check if weapon can attack with shield
            if ( canAttackWithShield == false && XMeleeMod.IsUsingShield(byEntity) )
            {
                (byEntity.World.Api as ICoreClientAPI)?.TriggerIngameError(this, "ingameerror-attack-with-shield", Lang.Get("xmelee:ingameerror-attack-with-shield"));
                handling = EnumHandHandling.PreventDefault;
                return;
            }
            
            int attackDelayMilliseconds = (int) (attackDelay * 1000f);
            int attackWindowMilliseconds = (int) (attackWindow * 1000f);
            int attackTimeMilliseconds = (int) (attackTime * 1000f);

            byEntity.StartAnimation(base.GetHeldTpHitAnimation(itemslot, byEntity));
            byEntity.Attributes.SetInt("didattack", 0);
            byEntity.Stats.Set("walkspeed", "xmelee", walkspeedModifier, true); // slow movement while attacking

            // play strike sound
            byEntity.World.RegisterCallback((dt) =>
            {
                IPlayer byPlayer = (byEntity as EntityPlayer).Player;
                if (byPlayer == null) return;

                if (byEntity.Controls.HandUse == EnumHandInteract.HeldItemAttack)
                {
                    byPlayer.Entity.World.PlaySoundAt(new AssetLocation("sounds/player/strike"), byPlayer.Entity, byPlayer, 0.9f + (float)api.World.Rand.NextDouble() * 0.2f, 16, 1f);
                }
            }, attackDelayMilliseconds);

            // disable animation after window
            byEntity.World.RegisterCallback((dt) => {
                byEntity.StopAnimation(base.GetHeldTpHitAnimation(itemslot, byEntity));
            }, attackWindowMilliseconds);

            // finish attack
            byEntity.World.RegisterCallback((dt) =>
            {
                byEntity.Stats.Set("walkspeed", "xmelee", 0f, true);
                byEntity.Attributes.SetInt("didattack", 1);
            }, attackTimeMilliseconds);

            handling = EnumHandHandling.PreventDefault;
        }

        public override bool OnHeldAttackCancel(float secondsUsed, ItemSlot itemslot, EntityAgent byEntity, BlockSelection blockSelection, EntitySelection entitySel, EnumItemUseCancelReason cancelReason)
        {
            return false;
        }

        /**
         * Run swing animation,
         * Hit single entity in front of player until swing is over.
         */
        public override bool OnHeldAttackStep(float secondsUsed, ItemSlot itemslot, EntityAgent byEntity, BlockSelection blockSelection, EntitySelection entitySel)
        {
            float attackDelay = DEFAULT_ATTACK_DELAY;
            float attackWindow = DEFAULT_ATTACK_WINDOW;
            float attackTime = DEFAULT_ATTACK_TIME;
            bool canAttackWithShield = DEFAULT_CAN_ATTACK_WITH_SHIELD;
            if (itemslot.Itemstack.Collectible.Attributes != null)
            {
                attackDelay = itemslot.Itemstack.Collectible.Attributes["attackDelay"].AsFloat(DEFAULT_ATTACK_DELAY);
                attackWindow = itemslot.Itemstack.Collectible.Attributes["attackWindow"].AsFloat(DEFAULT_ATTACK_WINDOW);
                attackTime = itemslot.Itemstack.Collectible.Attributes["attackTime"].AsFloat(DEFAULT_ATTACK_TIME);
                canAttackWithShield = itemslot.Itemstack.Collectible.Attributes["canAttackWithShield"].AsBool(DEFAULT_CAN_ATTACK_WITH_SHIELD);
            }

            // check if weapon can attack with shield
            if ( canAttackWithShield == false && XMeleeMod.IsUsingShield(byEntity) )
            {
                (byEntity.World.Api as ICoreClientAPI)?.TriggerIngameError(this, "ingameerror-attack-with-shield", Lang.Get("xmelee:ingameerror-attack-with-shield"));
                return false;
            }

            if (byEntity.World.Side == EnumAppSide.Client)
            {
                // time in attack motion
                float t0 = Math.Min(1f, secondsUsed/attackDelay); // ease-in
                float t1 = XMeleeMod.AnimationEnvelope(secondsUsed, attackDelay, attackDelay + 0.14f); // main
                float t2 = XMeleeMod.AnimationEnvelope(secondsUsed, attackTime - 0.10f, attackTime); // ease-out

                // model animation
                IClientWorldAccessor world = byEntity.World as IClientWorldAccessor;
                ModelTransform tf = new ModelTransform();
                tf.EnsureDefaultValues();

                tf.Origin.Set(0.5f, 0f, 0.5f);
                tf.Translation.Set(
                    t1 * 0.5f + t2 * -0.5f,
                    t0 * -0.6f + t1 * 0.4f,
                    t0 * 0.2f + t1 * -1.0f + t2 * 0.8f
                );
                tf.Rotation.Set(
                    t0 * 30f + t1 * -60f + t2 * 30f,
                    t0 * 20f + t1 * -12f,
                    t0 * -10f + t2 * 10f
                );

                byEntity.Controls.UsingHeldItemTransformBefore = tf;

                // do damage
                if (secondsUsed > attackDelay && secondsUsed < attackWindow)
                {
                    if ( entitySel != null && byEntity.Attributes.GetInt("didattack") == 0 )
                    {
                        world.TryAttackEntity(entitySel);
                        byEntity.Attributes.SetInt("didattack", 1); // mark attacked
                    }
                }
            }

            return secondsUsed < attackTime;
        }

        public override void OnHeldAttackStop(float secondsUsed, ItemSlot itemslot, EntityAgent byEntity, BlockSelection blockSelection, EntitySelection entitySel)
        {

        }


        public override void GetHeldItemInfo(ItemSlot itemslot, StringBuilder dsc, IWorldAccessor world, bool withDebugInfo)
        {
            base.GetHeldItemInfo(itemslot, dsc, world, withDebugInfo);
            if (itemslot.Itemstack.Collectible.Attributes == null) return;

            float attackDelay = DEFAULT_ATTACK_DELAY;
            float attackWindow = DEFAULT_ATTACK_WINDOW;
            float attackTime = DEFAULT_ATTACK_TIME;
            float attackWalkspeed = DEFAULT_ATTACK_WALK_SPEED_MODIFIER;
            float parryDelay = DEFAULT_PARRY_DELAY;
            float parryWindow = DEFAULT_PARRY_WINDOW;
            float parryTime = DEFAULT_PARRY_TIME;
            float parryWalkspeed = DEFAULT_PARRY_WALK_SPEED_MODIFIER;
            if ( itemslot.Itemstack.Collectible.Attributes != null )
            {
                attackDelay = itemslot.Itemstack.Collectible.Attributes["attackDelay"].AsFloat(DEFAULT_ATTACK_DELAY);
                attackWindow = itemslot.Itemstack.Collectible.Attributes["attackWindow"].AsFloat(DEFAULT_ATTACK_WINDOW);
                attackTime = itemslot.Itemstack.Collectible.Attributes["attackTime"].AsFloat(DEFAULT_ATTACK_TIME);
                attackWalkspeed = itemslot.Itemstack.Collectible.Attributes["attackWalkspeed"].AsFloat(DEFAULT_ATTACK_WALK_SPEED_MODIFIER);

                parryDelay = itemslot.Itemstack.Collectible.Attributes["parryDelay"].AsFloat(DEFAULT_PARRY_DELAY);
                parryWindow = itemslot.Itemstack.Collectible.Attributes["parryWindow"].AsFloat(DEFAULT_PARRY_WINDOW);
                parryTime = itemslot.Itemstack.Collectible.Attributes["parryTime"].AsFloat(DEFAULT_PARRY_TIME);
                parryWalkspeed = itemslot.Itemstack.Collectible.Attributes["parryWalkspeed"].AsFloat(DEFAULT_PARRY_WALK_SPEED_MODIFIER);
            }

            dsc.AppendLine(Lang.Get("xmelee:desc-attack-time") + String.Format(": {0}/{1}/{2} s", attackDelay, attackWindow, attackTime));
            dsc.AppendLine(Lang.Get("xmelee:desc-attack-walkspeed") + ": " + attackWalkspeed);
            dsc.AppendLine(Lang.Get("xmelee:desc-parry-time") + String.Format(": {0}/{1}/{2} s", parryDelay, parryWindow, parryTime));
            dsc.AppendLine(Lang.Get("xmelee:desc-parry-walkspeed") + ": " + parryWalkspeed);
        }

        public override WorldInteraction[] GetHeldInteractionHelp(ItemSlot itemslot)
        {
            return new WorldInteraction[] {
                new WorldInteraction()
                {
                    ActionLangCode = "heldhelp-throw",
                    MouseButton = EnumMouseButton.Right,
                }
            }.Append(base.GetHeldInteractionHelp(itemslot));
        }
    }
}
