using ProtoBuf;

namespace XMelee
{
    /**
     * Packet from server to client indicating stun time in milliseconds
     */
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class PacketServerToClientStun
    {
        public int length;
    }

    /**
     * Packet from server to client indicating successful parry
     */
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class PacketServerToClientParry
    {
        
    }
}