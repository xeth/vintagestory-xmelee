using System;
using System.Collections.Generic;
using System.Text;
using Vintagestory.API;
using Vintagestory.API.Client;
using Vintagestory.API.Common;
using Vintagestory.API.Common.Entities;
using Vintagestory.API.Config;
using Vintagestory.API.Datastructures;
using Vintagestory.API.MathTools;

namespace XMelee
{
    public class EntityBehaviorStun : EntityBehavior
    {
        EntityPlayer entityPlayer;

        public EntityBehaviorStun(Entity entity) : base(entity)
        {
            if ( entity is EntityPlayer ) 
            {
                entityPlayer = entity as EntityPlayer;
            }
        }

        public override void Initialize(EntityProperties properties, JsonObject typeAttributes)
        {
            
        }

        public override void OnGameTick(float deltaTime)
        {
            EnumAppSide appSide = entity.World.Side;

            if ( appSide == EnumAppSide.Client )
            {
                IClientWorldAccessor world = entity.World as IClientWorldAccessor;
                world.AddCameraShake(0.02f);
                entityPlayer.TryStopHandAction(true, EnumItemUseCancelReason.ReleasedMouse);
                entityPlayer.Attributes.SetInt("didattack", 1);
            }
            // TODO: do we need anything on server side?
            else if ( appSide == EnumAppSide.Server )
            {
                entityPlayer.TryStopHandAction(true, EnumItemUseCancelReason.ReleasedMouse);
                entityPlayer.Attributes.SetInt("didattack", 1);
            }
        }

        public override void GetInfoText(StringBuilder infotext)
        {
           
        }

        public override string PropertyName()
        {
            return "stun";
        }
    }
}
