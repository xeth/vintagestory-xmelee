﻿using System;
using Vintagestory.API;
using Vintagestory.API.Client;
using Vintagestory.API.Common;
using Vintagestory.API.Common.Entities;
using Vintagestory.API.Config;
using Vintagestory.API.Datastructures;
using Vintagestory.API.MathTools;
using Vintagestory.API.Server;
using Vintagestory.API.Util;
using Vintagestory.GameContent;

[assembly: ModInfo( "xmelee",
    Description = "Melee mod",
    Website     = "",
    Authors     = new []{ "xeth" } )]

namespace XMelee
{
    public class XMeleeMod : ModSystem
    {
        public static string PATH { get; } = "xmelee";

        public static string CONFIG_PATH { get; } = "xmeleeconfig.json";

        public static XMeleeConfig Config = new XMeleeConfig();

        // server
        ICoreServerAPI sapi;
        IServerNetworkChannel serverChannel;

        // client
        ICoreClientAPI capi;
        IClientNetworkChannel clientChannel;


        /**
         * Return x = [0.0, 1.0] linearly interpolated from start to end
         */
        internal static float AnimationEnvelope(float t, float start, float end)
        {
            if ( t <= start )
            {
                return 0f;
            }
            else if ( t >= end )
            {
                return 1f;
            }
            else
            {
                return (t - start) / (end - start);
            }
        }

        /**
         * Utility function, return if entity offhand is a shield
         */
        internal static bool IsUsingShield(EntityAgent entity)
        {
            return ( entity.LeftHandItemSlot.Itemstack?.Item is ItemShield );
        }

        public override void Start(ICoreAPI api)
        {
            api.RegisterItemClass(ItemLongsword.NAME, typeof(ItemLongsword));
            api.RegisterItemClass(ItemPolearm.NAME, typeof(ItemPolearm));
            api.RegisterItemClass(ItemPike.NAME, typeof(ItemPike));
            api.RegisterItemClass(ItemRapier.NAME, typeof(ItemRapier));
            api.RegisterItemClass(ItemMace.NAME, typeof(ItemMace));

            // load config
            try
            {
                XMeleeConfig loadedConfig = api.LoadModConfig<XMeleeConfig>(XMeleeMod.CONFIG_PATH);
                if ( loadedConfig != null )
                {
                    XMeleeMod.Config = loadedConfig;
                }
                else
                {
                    api.StoreModConfig(XMeleeMod.Config, CONFIG_PATH);
                }
            }
            catch ( System.Exception e )
            {
                System.Console.WriteLine("{0} Failed to load mod config.", e);
            }
        }

        public override void StartClientSide(ICoreClientAPI api)
        {
            capi = api;

            clientChannel =
                api.Network.RegisterChannel("xmelee")
                .RegisterMessageType(typeof(PacketServerToClientStun))
                .RegisterMessageType(typeof(PacketServerToClientParry))
                .SetMessageHandler<PacketServerToClientStun>(OnClientReceivePacketStun)
                .SetMessageHandler<PacketServerToClientParry>(OnClientReceivePacketParry)
            ;
        }

        public override void StartServerSide(ICoreServerAPI api)
        {
            sapi = api;
            api.Event.PlayerJoin += ServerEventPlayerJoin;
            api.Event.OnPlayerInteractEntity += ServerEventOnInteractEntity;

            serverChannel =
                api.Network.RegisterChannel("xmelee")
                .RegisterMessageType(typeof(PacketServerToClientStun))
                .RegisterMessageType(typeof(PacketServerToClientParry))
            ;
        }

        private void ServerEventOnInteractEntity(Entity entity, IPlayer byPlayer, ItemSlot slot, Vec3d hitPosition, int mode, ref EnumHandling handling)
        {
            // Console.WriteLine("ServerEventOnInteractEntity");
            // Console.WriteLine(entity);
            // Console.WriteLine(byPlayer);
            // Console.WriteLine(slot);
            EntityPlayer entityPlayer = byPlayer.Entity;
            if ( entityPlayer.Attributes.GetInt("parrystun", 0) == 1 )
            {
                handling = EnumHandling.PreventSubsequent;
            }
        }

        private void ServerEventPlayerJoin(IServerPlayer byPlayer)
        {
            var bh = byPlayer.Entity.GetBehavior<EntityBehaviorHealth>();
            if ( bh != null )
            {
                bh.onDamaged += (dmg, dmgSource) => ServerHandleParry(byPlayer.Entity, dmg, dmgSource);
            }
        }

        /**
         * Handles packet indicating player attack was parried, apply stun
         * onto player.
         */
        private void OnClientReceivePacketStun(PacketServerToClientStun msg)
        {
            int stunLengthMillis = msg.length;
            EntityPlayer entity = capi.World.Player.Entity;
            if ( entity != null )
            {
                EntityBehaviorStun stunBehavior = new EntityBehaviorStun(entity);
                entity.AddBehavior(stunBehavior);
                entity.World.RegisterCallback((dt) =>
                {
                    entity.RemoveBehavior(stunBehavior);
                    entity.Attributes.SetInt("didattack", 0);
                }, stunLengthMillis);
                
                entity.StopHandAnims();
            }

            // camera shake
            IClientWorldAccessor world = capi.World;
            world.AddCameraShake(0.35f);
        }

        /**
         * Handles packet indicating successful parry
         */
        private void OnClientReceivePacketParry(PacketServerToClientParry msg)
        {
            IClientWorldAccessor world = capi.World;
            world.AddCameraShake(0.35f);
        }

        private float ServerHandleParry(EntityPlayer entity, float damage, DamageSource dmgSource)
        {
            Entity srcEntity = dmgSource.SourceEntity;

            // only block EntityAgent types for now, e.g. projectiles are ignored
            // TODO: config setting to toggle other entity types (e.g. to allow parrying arrows)
            if ( srcEntity != null && srcEntity is EntityAgent )
            {
                int blocking = entity.Attributes.GetInt("parry", 0);
                if ( blocking == 1 )
                {
                    IServerWorldAccessor world = entity.World as IServerWorldAccessor;

                    // srcEntity = entity; // DEBUG ONLY
                    
                    if ( srcEntity is EntityPlayer && srcEntity.Attributes.GetInt("parrystun") == 0 )
                    {
                        EntityPlayer srcPlayer = srcEntity as EntityPlayer;
                        srcPlayer.StartAnimation("Hurt");
                        srcPlayer.StopHandAnims();
                        srcPlayer.Attributes.SetInt("parrystun", 1);

                        // get stunned time based on item in hand
                        float stunTime = XMeleeMod.Config.DefaultStunTime;

                        ItemStack itemStack = srcPlayer.RightHandItemSlot.Itemstack;
                        if ( itemStack != null && itemStack.Collectible.Attributes != null )
                        {
                            stunTime = itemStack.Collectible.Attributes["stunnedTime"].AsFloat(XMeleeMod.Config.DefaultStunTime);
                        }
                        
                        int stunTimeMillis = (int) (stunTime * 1000);
                        EntityBehaviorStun stunBehavior = new EntityBehaviorStun(entity);
                        
                        // must add stun in callback because this handler runs while
                        // behaviors is being iterated (so avoid modifying behaviors)
                        world.RegisterCallback((dt) => 
                        {
                            srcPlayer.AddBehavior(stunBehavior);
                        }, 0);
                        world.RegisterCallback((dt) =>
                        {
                            srcPlayer.StopAnimation("Hurt");
                            srcPlayer.RemoveBehavior(stunBehavior);
                            srcPlayer.Attributes.SetInt("didattack", 0);
                            srcPlayer.Attributes.SetInt("parrystun", 0);
                        }, stunTimeMillis);

                        // send stun packet to player who attacked
                        serverChannel.SendPacket(new PacketServerToClientStun()
                        {
                            length = stunTimeMillis,
                        }, srcPlayer.Player as IServerPlayer);
                    }

                    // stops player damage red effects on side
                    entity.WatchedAttributes.SetInt("onHurtCounter", entity.WatchedAttributes.GetInt("onHurtCounter") + 1);
                    entity.WatchedAttributes.SetFloat("onHurt", 0f); // Causes the client to be notified

                    // play block sound
                    entity.World.PlaySoundAt(new AssetLocation("sounds/effect/anvilmergehit"), entity, null, false, 16, 1f);
                    
                    // send successful parry packet
                    serverChannel.SendPacket(new PacketServerToClientParry(), entity.Player as IServerPlayer);

                    // reduce damage to 0f
                    return 0f;
                }
                // enemy is still stunned, set damage to 0
                else if ( srcEntity.Attributes.GetInt("parrystun", 0) == 1 )
                {
                    // stops player damage red effects on side
                    entity.WatchedAttributes.SetInt("onHurtCounter", entity.WatchedAttributes.GetInt("onHurtCounter") + 1);
                    entity.WatchedAttributes.SetFloat("onHurt", 0f); // Causes the client to be notified
                    return 0f;
                }
            }

            return damage;
        }

    }

    /**
     * Json mod config 
     */
    public class XMeleeConfig
    {
        // default stun time after getting hit
        public float DefaultStunTime = 2.0f;
    }
}
