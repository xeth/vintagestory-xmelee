Melee weapon combat system vintage story mod
=====================================
Melee weapons and combat system inspired by mordhau.

**Controls**:
- Left click: attack
- Right click: parry

**Mechanics**:  
Weapons can attack and parry, both use time windows.
Each time window has a wind up, attack window, and ease out:
```
   attack window
           |
      _____
     /     \
  __/       \___    -> time
      |     |
 wind up    total attack time
```
Weapons only do damage during `wind up < t < attack window`.
Attacks only hit targeted enemies you are looking at during swing
period. Parrying blocks any melee attack received during its window
(including from behind, there's no directional detection).
Successful parry stuns the attacker for a small period and prevents
them from attacking/parrying.

Certain weapons can/cannot attack or parry with a shield equipped 
in left hand (see table).

Default weapon stats (for steel), A = attack, P = parry:
| Weapon     | Range | Damage | Tier | A. Delay | A. Window | A. Time | A. Walkspeed | Shield? |
|:-----------|:-----:|:------:|:----:|:--------:|:---------:|:-------:|:------------:|:--------|
| Rapier     |  2.5  |   6    |  5   |   0.35   |   0.65    |  0.70   |    -0.20     | Yes     |
| Messer     |  2.5  |   6    |  5   |   0.40   |   0.60    |  0.70   |    -0.40     | Yes     |
| Mace       |  2.5  |   6    |  6   |   0.40   |   0.70    |  0.80   |    -0.40     | Yes     |
| Longsword  |  3.0  |   7    |  5   |   0.40   |   0.70    |  0.80   |    -0.60     | No      |
| Zweihander |  3.5  |   7    |  5   |   0.45   |   0.80    |  0.90   |    -0.70     | No      |
| Spear      |  4.0  |   6    |  5   |   0.45   |   0.90    |  1.00   |    -1.00     | Yes     |
| Halberd    |  4.5  |   7    |  5   |   0.45   |   0.90    |  1.00   |    -1.00     | No      |
| Pike       |  6.0  |   7    |  5   |   0.45   |   1.00    |  1.20   |    -1.00     | No      |

| Weapon     | P. Delay | P. Window | P. Time | P. Walkspeed | Stunned Time | Notes                 |
|:-----------|:--------:|:---------:|:-------:|:------------:|:------------:|:----------------------|
| Rapier     |   0.05   |   0.60    |  0.70   |    -0.20     |     1.0      |                       |
| Messer     |   0.05   |   0.60    |  0.70   |    -0.30     |     1.0      | Hit multiple targets  |
| Mace       |   0.05   |   0.60    |  0.70   |    -0.30     |     1.0      | Hit multiple targets  |
| Longsword  |   0.05   |   0.60    |  0.70   |    -0.40     |     1.1      | Hit multiple targets  |
| Zweihander |   0.08   |   0.60    |  0.80   |    -0.60     |     1.1      | Hit multiple targets  |
| Spear      |   0.05   |   0.60    |  0.70   |    -0.40     |     1.1      |                       |
| Halberd    |   0.05   |   0.60    |  0.70   |    -0.40     |     1.1      |                       |
| Pike       |    -     |     -     |    -    |      -       |     1.1      | No parry              |

Weapons only come in iron, meteoric iron, and steel variants.
Check crafting in-game for recipes.


Dev stuff
=====================================
This is setup for development from VS Code.

1. Download C#/dotnet/VS code plugins: <https://code.visualstudio.com/docs/languages/dotnet>

2. Launch run task in VS code (clr or mono)

3. Build actual release .zip using:  
`dotnet build -c release`


Credits/Contributions
=====================================
- [Craluminum2413](https://gitlab.com/Craluminum2413): Russian + Ukrainian translation